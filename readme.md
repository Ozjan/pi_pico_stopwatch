### Stoper na Raspberry Pi Pico

Projekt realizuje funkcjonalność stopera. Obsługuje 4-pozycyjny wyświetlacz 7-segmentowy oraz 3 przyciski.
Role przycisków są następujące:
 - start/stop
 - przyspiesz
 - zwolnij

Gdy stoper jest w trybie start, nalicza czas z określoną prędkością. Po przepełnieniu licznika zaczyna naliczać od zera.