if (TARGET tinyusb_device)
        add_executable(7seg
        main.cpp functions.cpp
        )

        # pull in common dependencies
        target_link_libraries(7seg pico_stdlib 
        pico_cyw43_arch_none)

        # create map/bin/hex file etc.
        pico_add_extra_outputs(7seg)

        pico_enable_stdio_usb(7seg 0)
        pico_enable_stdio_uart(7seg 1)
elseif(PICO_ON_DEVICE)
        message(WARNING "not building 7seg because TinyUSB submodule is not initialized in the SDK")
endif()