#pragma once
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"


extern const int intervals[10]; // mozliwe czasy timera
extern const uint led_pin;
extern const uint dig[4]; // cyfry od najstarszej
extern const uint seg[8]; // segmenty a,b,c,d,e,f,g,h
extern const uint buttons[3]; // plus, minus, stop
extern const bool digits[11][7]; // definicje konkretnych cyfr na wyswietlaczu

extern int sum;
extern int interval;
extern bool stop;
extern bool show_sleep;
extern bool buttons_prev[3];
extern struct repeating_timer timer_inc;

bool buttons_callback(struct repeating_timer *t);
bool repeating_timer_callback(struct repeating_timer *t);
bool show_callback(struct repeating_timer *t);
bool increment_callback(struct repeating_timer *t);

void setDigit(int a);
void showAll(uint liczba, bool temp);

void plus(int &liczba);
void minus(int &liczba);
bool pressed(int button);
void update(int button);