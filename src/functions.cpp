#include "functions.h"
#include <algorithm>

#pragma region const
const int intervals[10] = {1, 2, 5, 10, 20, 50, 100, 200, 500, 1000};

const uint led_pin = CYW43_WL_GPIO_LED_PIN;
const uint dig[4] = {9, 20, 21, 16};
const uint seg[8] = {11, 10, 26, 18, 19, 12, 22, 17};
const uint buttons[3] = {15,13,14};
const uint button_minus = 13;
const uint button_calc = 14;
const bool digits[11][7] =  {{0, 0, 0, 0, 0, 0, 1},  //0
                            {1, 0, 0, 1, 1, 1, 1},  //1
                            {0, 0, 1, 0, 0, 1, 0},  //2
                            {0, 0, 0, 0, 1, 1, 0},  //3
                            {1, 0, 0, 1, 1, 0, 0},  //4
                            {0, 1, 0, 0, 1, 0, 0},  //5
                            {0, 1, 0, 0, 0, 0, 0},  //6
                            {0, 0, 0, 1, 1, 1, 1},  //7
                            {0, 0, 0, 0, 0, 0, 0},  //8
                            {0, 0, 0, 0, 1, 0, 0},  //9
                            {1, 1, 1, 1, 1, 1, 1}}; //pusto

#pragma endregion const

int sum = 1376;
int interval = 3;
bool stop = false;
bool show_sleep = true;
bool buttons_prev[3] = {1,1,1};
struct repeating_timer timer_inc;

bool buttons_callback(struct repeating_timer *t)
{
    for (size_t i = 0; i < 3; i++)
    {
        if (pressed(i))
        {
            switch (i)
            {
            case 0: minus(interval); break;
            case 1: plus(interval); break;
            case 2: stop = !stop; break;
            }
        }
        
    }
    
    return true;
}

bool repeating_timer_callback(struct repeating_timer *t) {
    cyw43_arch_gpio_put(led_pin, !cyw43_arch_gpio_get(led_pin));
    return true;
}

bool show_callback(struct repeating_timer *t)
{
    show_sleep = false;
    return true;
}

bool increment_callback(repeating_timer *t)
{
    if (!stop)
        sum++;
    if (sum == 10000)
        sum = 0;
    return true;
}

// ustawia odpowiednie piny, aby wyświetlić cyfrę a na wyświetlaczu
void setDigit(int a)
{
    for (int i = 0; i < 7; i++)
    {
        gpio_put(seg[i], digits[a][i]);
    }
}

// wyświetla całą liczbę na 4-pozycyjnym wyświetlaczu 7-segmentowym
void showAll(uint number, bool dot)
{
    uint a = number;
    int dzielnik = 1000;
    bool pocz = false;
    gpio_put(seg[7], true);
    struct repeating_timer timer_show;

    add_repeating_timer_us(-std::min(intervals[interval]*250,2500), show_callback, NULL, &timer_show);


    for (size_t i = 0; i < 4; i++)
    {
        while (show_sleep)
        {
            tight_loop_contents();
        }
        
        if (i == 0)
            gpio_put(dig[3], false);
        else
            gpio_put(dig[i-1], false);

        int cyfra = a/(dzielnik);
        if (cyfra > 0)
            pocz = true;
        
        if (i == 3)
        {
            setDigit(cyfra);
            gpio_put(seg[7], !dot);
        }
        else if (pocz)
            setDigit(cyfra);
        else
            setDigit(10);
        
        a = a % dzielnik;
        dzielnik /= 10;

        gpio_put(dig[i], true);

        show_sleep = true;
    }
    cancel_repeating_timer(&timer_show);
    
}

bool pressed(int button)
{
    if (button > 2)
        return false;

    if (!gpio_get(buttons[button]) && buttons_prev[button])
    {
        update(button);
        return true;
    }
    else
    {
        update(button);
        return false;
    }
    
    
}

void update(int button)
{
    buttons_prev[button] = gpio_get(buttons[button]);
}

void plus(int &liczba)
{
    if (liczba < 9)
    {
        liczba++;
        cancel_repeating_timer(&timer_inc);
        add_repeating_timer_ms(-intervals[interval], increment_callback, NULL, &timer_inc);
    }
}

void minus(int &liczba)
{
    if (liczba > 0)
    {
        liczba--;
        cancel_repeating_timer(&timer_inc);
        add_repeating_timer_ms(-intervals[interval], increment_callback, NULL, &timer_inc);
    }
}