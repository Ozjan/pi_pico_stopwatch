#include <stdio.h>
#include "functions.h"

int main(){

    //inicjalizacja pinow
    #pragma region pin_init
    // stdio_init_all();
    for (size_t i = 0; i < 4; i++)
    {
        gpio_init(dig[i]);
        gpio_set_dir(dig[i], GPIO_OUT);
    }

    for (size_t i = 0; i < 8; i++)
    {
        gpio_init(seg[i]);
        gpio_set_dir(seg[i], GPIO_OUT);
    }

    for (size_t i = 0; i < 3; i++)
    {
        gpio_init(buttons[i]);
        gpio_set_dir(buttons[i], GPIO_IN);
        gpio_pull_up(buttons[i]);
    }

    stdio_init_all();
    cyw43_arch_init();
    #pragma endregion pin_init

    printf("Program started\n");


    cyw43_arch_gpio_put(led_pin, true);
    gpio_put(seg[7], true);
    
    struct repeating_timer timer_led, timer_button;
    add_repeating_timer_ms(-1000, repeating_timer_callback, NULL, &timer_led);
    add_repeating_timer_ms(-20, buttons_callback, NULL, &timer_button);
    add_repeating_timer_ms(-intervals[interval], increment_callback, NULL, &timer_inc);
    

    while (true)
    {
        showAll(sum, false);
    }
    
}